package com.example.ankit.contactsmanager.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ankit.contactsmanager.R;
import com.example.ankit.contactsmanager.utils.Utils;

import java.io.File;
import java.io.IOException;

public class AddContactActivity extends AppCompatActivity {

    private ImageView mIvPersonPhoto;

    private Uri mCameraPhotoUri;
    private String mCurrentPhotoPath;

    private static final int REQUEST_PHOTO_GALLERY = 1000;
    private static final int REQUEST_PHOTO_CAMERA = 1001;

    private static final int REQUEST_EXTERNAL_STORAGE_PERMISSION = 2000;
    private static final int REQUEST_CAMERA_PERMISSION = 2001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        //set up action bar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        //setup views
        Button btnChoosePhoto = (Button) findViewById(R.id.btn_choose_photo);
        if (btnChoosePhoto != null) {
            btnChoosePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //check for external storage permission
                    if (ActivityCompat.checkSelfPermission(AddContactActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        //if camera is present then show choose photo dialog else open gallery
                        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                            showChoosePhotoDialog();
                        } else {
                            getPhotoFromGallery();
                        }
                    } else {
                        //ask for external storage permission permission and then show choose photo dialog
                        askExternalStoragePermission();
                    }
                }
            });
        }

        mIvPersonPhoto = (ImageView) findViewById(R.id.iv_person_photo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                validateAndFinish();
                break;

            case R.id.action_discard_changes:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        validateAndFinish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_PHOTO_CAMERA:
                Utils.addPicToGallery(this, mCameraPhotoUri);
                setPic();
                break;

            case REQUEST_PHOTO_GALLERY:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    mCurrentPhotoPath = Utils.getRealPathFromURI_API19(this, data.getData());
                } else {
                    mCurrentPhotoPath = Utils.getRealPathFromURI_BelowAPI19(this, data.getData());
                }
                setPic();
                break;

            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case REQUEST_EXTERNAL_STORAGE_PERMISSION:
                //if external storage permission is granted then show choose photo dialog
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //if camera is present then show choose photo dialog else open gallery
                    if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                        showChoosePhotoDialog();
                    } else {
                        getPhotoFromGallery();
                    }
                }
                break;

            case REQUEST_CAMERA_PERMISSION:
                //if camera permission is granted then start camera app
                getPhotoFromCamera();
                break;

            default:
                break;
        }
    }

    private void askExternalStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("External storage permission is necessary");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(AddContactActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_EXTERNAL_STORAGE_PERMISSION);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(AddContactActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_EXTERNAL_STORAGE_PERMISSION);
        }
    }

    private void askCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Camera permission is necessary");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(AddContactActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(AddContactActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    private void showChoosePhotoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.choose_photo)
                .setPositiveButton(R.string.option_gallery, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPhotoFromGallery();
                    }
                })
                .setNegativeButton(R.string.option_camera, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //check if camera permission is there then open camera app else ask for permission
                        if (ActivityCompat.checkSelfPermission(AddContactActivity.this,
                                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            getPhotoFromCamera();
                        } else {
                            //ask for camera permission permission and then open camera app
                            askCameraPermission();
                        }
                    }
                });

        builder.show();
    }

    private void getPhotoFromGallery() {
        Intent intent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        } else {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
        }
        intent.setType("image/*");

        startActivityForResult(intent, REQUEST_PHOTO_GALLERY);
    }

    private void getPhotoFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = Utils.createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();

                Toast.makeText(this, R.string.common_error_message, Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCameraPhotoUri = Uri.fromFile(photoFile);
                mCurrentPhotoPath = photoFile.getAbsolutePath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCameraPhotoUri);
                startActivityForResult(takePictureIntent, REQUEST_PHOTO_CAMERA);
            }
        }
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = mIvPersonPhoto.getWidth();
        int targetH = mIvPersonPhoto.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mIvPersonPhoto.setImageBitmap(bitmap);
    }

    private void validateAndFinish() {

        String name = null;
        EditText etName = (EditText) findViewById(R.id.et_name);
        if (etName != null) {
            name = etName.getText().toString().trim();
        }

        String nickName;
        EditText etNickName = (EditText) findViewById(R.id.et_nick_name);
        if (etNickName != null) {
            nickName = etNickName.getText().toString().trim();
        }

        finish();
    }
}
